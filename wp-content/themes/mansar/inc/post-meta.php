<?php ?>		
<p class="post-meta vsmall">
	<span class="post-meta-date"><?php _e( 'Atualizado em ' , 'mansar' ); ?><?php echo the_time(get_option( 'date_format' )) ?></span>
	<span class="post-meta-author"><?php _e( 'Por ' , 'mansar' ); ?><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) )?>" title="<?php sprintf( esc_attr__( 'Ver todos os posts de %s', 'mansar' ), get_the_author() ) ?>"><?php echo get_the_author() ?> </a></span>
	<?php if ( post_password_required() != true ): ?>
	    <span class="post-meta-comments"><?php comments_popup_link( __( 'Deixe um comentário', 'mansar' ), __( '1 Comentário', 'mansar' ), __( '% Comentários', 'mansar' ) ); ?></span>
    <?php endif; ?>
</p>
<div class="clear"></div>
<?php ?>