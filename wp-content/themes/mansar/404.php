<?php get_header(); ?>
	<section id="content" class="first clearfix" role="main">
		<div class="post-container">
		    <div class="singlebox">
			  <div class="not-found-block center">
	                <h1><?php _e('A página procurada não existe.', 'mansar'); ?></h1>
					<h3><?php _e('Lamentamos muito pela inconveniência.', 'mansar'); ?></h3>
	         <p><?php _e('Tente usar a caixa de pesquisa abaixo.', 'mansar'); ?></p>
	                    <form role="search" method="get" id="" action="<?php echo home_url(); ?>/">
	                        <input type="text" value="" name="s" id="s">
                            <input class="button" type="submit" id="searchsubmit" value="<?php _e('Search', 'mansar'); ?>">
						</form>
					   <p><?php _e('Ou', 'mansar'); ?></p>
					   <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('Ir para a Página Inicial', 'mansar'); ?></a>
			  </div>
			</div>
		</div>
	</section> <!-- end #main -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>