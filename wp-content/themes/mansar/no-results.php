<?php
/* The template part for displaying a message that posts cannot be found. @package mansar */
?>
<div class="singlebox">
  <div class="not-found-block center">
        <h3>Oops..! Nenhum resultado encontrado.</h3>
            <p>Tente buscar com diferentes palavras-chaves. </p>	                                   
                <form role="search" method="get" id="" action="<?php echo home_url(); ?>/">
                    <input type="text" value="" name="s" id="s">
                    <input class="button" type="submit" id="searchsubmit" value="Buscar">
				</form>
		    <p>Ou</p>
		   <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>">Ir para a Página Inicial</a>
  </div>
</div>