<?php
# Get Theme Options
/*-------------------------------*/
function mansar_get_option( $name ) {
	$get_options = get_option( 'mansar_options' );
	
	if( !empty( $get_options[$name] ))
		return $get_options[$name];
		
	return false ;
}

# Get Other Templates 
/*-----------------------------*/
function mansar_include($template){
	get_template_part ( get_template_directory() . '/inc/'.$template.'.php' );
}

#Excerpt Length
function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

# Site Title
function mansar_wp_title( $title, $sep ) {
	if ( is_feed() )
		return $title;
	$title .= get_bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";
	return $title;
}
add_filter( 'wp_title', 'mansar_wp_title', 10, 2 );

# No Title
function mansar_the_title ( $title ) {

	if ( in_the_loop() && ! is_page() ) {
		if ( ! $title )
			$title = __( 'Untitled', 'mansar' );
	}
	return $title;

}
add_filter( 'the_title', 'mansar_the_title' );

# Comments Function (Do not Edit)  
/*-------------------------- */
if ( ! function_exists( 'mansar_comment' ) ) :

function mansar_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e( 'Pingback:', 'mansar' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'mansar' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
		
			<div class="comment-meta">
				<figure class="comment-avatar">
    	            <?php echo get_avatar( $comment, 48 ); ?>
                </figure>	
				
				<div class="comment-metadata">
				    <?php comment_author_link(); ?>
                    <span class="datetime"><?php comment_date('F j, Y'); ?></span>					
					<?php edit_comment_link( __( 'Editar', 'mansar' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .comment-metadata -->

				<?php if ( '0' == $comment->comment_approved ) : ?>
       				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'mansar' ); ?></p>
				<?php endif; ?>
		        <div class="comment-content">
				    <?php comment_text(); ?>
			    </div><!-- .comment-content ends -->
                 
				<div class="reply">
				    <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			    </div><!-- .reply --> 
            </div><!-- .comment-meta ends-->
		<!-- .comment-body -->
	<?php
	endif;
}
endif; // ends check for mansar_comment()

# Breadcrumbs
/* ------------------------- */
function mansar_breadcrumbs() {

  $delimiter = mansar_get_option('breadcrumbs_delimiter') ? mansar_get_option('breadcrumbs_delimiter') : '&raquo;';
  $before = '<span class="current">';
  $after = '</span>';
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<div itemscope itemtype="http://schema.org/WebPage" id="crumbs">'.__( 'Você está aqui:' , 'mansar' );
 
    global $post;
    $homeLink = home_url();
    echo ' <a itemprop="breadcrumb" href="' . $homeLink . '">' . __( 'Home' , 'mansar' ) . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0){
		$cat_code = get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ');
		echo $cat_code = str_replace ('<a','<a itemprop="breadcrumb"', $cat_code );
	  }
      echo $before . '' . single_cat_title('', false) . '' . $after;
 
    } elseif ( is_day() ) {
      echo '<a itemprop="breadcrumb" href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a itemprop="breadcrumb"  href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a itemprop="breadcrumb" href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a itemprop="breadcrumb" href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cat_code = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
		echo $cat_code = str_replace ('<a','<a itemprop="breadcrumb"', $cat_code );

        echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); 
	  if(isset($cat[0])){
	  $cat = $cat[0];}
      echo '<a itemprop="breadcrumb" href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a itemprop="breadcrumb" href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_search() ) {
      echo $before ;
	  printf( __( 'Search Results for: %s', 'mansar' ),  get_search_query() );
	  echo  $after;
 
    } elseif ( is_tag() ) {
	  echo $before ;
	  printf( __( 'Tag Archives: %s', 'mansar' ), single_tag_title( '', false ) );
	  echo  $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before ;
	  printf( __( 'Author Archives: %s', 'mansar' ),  $userdata->display_name );
	  echo  $after;
 
    } elseif ( is_404() ) {
      echo $before;
	  _e( 'Not Found', 'mansar' );
	  echo  $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('page ' , 'mansar') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
   } }

# Queue Comments Reply
/*-------------------------*/
function mansar_comments_queue_js(){
if ( (!is_admin()) && is_singular() && comments_open() )
  wp_enqueue_script( 'comment-reply' );
}
add_action('wp_print_scripts', 'mansar_comments_queue_js');

# Theme Scripts
/*-------------------------*/
function mansar_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_script( 'mansar-common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array( 'jquery' ) );	 
    wp_register_style('googleFonts1', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700');
	wp_register_style('googleFonts2', 'http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic');
    wp_enqueue_style( 'googleFonts1');
	wp_enqueue_style( 'googleFonts2');
	}
add_action( 'wp_enqueue_scripts', 'mansar_scripts' );


# Get Most Recent posts from Category
/*-------------------------*/
function mansar_last_posts_cat($numberOfPosts = 5 , $cats = 1){
	global $post;
	$orig_post = $post;

	$lastPosts = get_posts('category='.$cats.'&numberposts='.$numberOfPosts);
	foreach($lastPosts as $post): setup_postdata($post);
?>
<li class="mbottom">
	<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) : ?>			
		<div class="post-thumbnail mright">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'mansar' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail('thumbnail'); ?></a>
		</div><!-- post-thumbnail /-->
	<?php endif; ?>
	<p><a href="<?php the_permalink(); ?>"><?php the_title();?></a></p>
</li>
<?php endforeach;
	$post = $orig_post;
}
?>